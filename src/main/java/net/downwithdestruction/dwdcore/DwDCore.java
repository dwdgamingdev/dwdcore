package net.downwithdestruction.dwdcore;

import net.downwithdestruction.dwdcore.players.DwDPlayerManager;
import org.bukkit.Bukkit;


public class DwDCore extends DwDPlugin {
    
    private static DwDCore instance;
    private static DwDPlayerManager playerManager;
    
    public static DwDCore getInstance() {
        return instance;
    }
    
    @Override
    public DwDPlayerManager getPlayerManager() {
        return playerManager;
    }

    @Override
    public void pluginEnable() {
        DwDCore.instance = this;
        DwDCore.playerManager = new DwDPlayerManager();
        Bukkit.getPluginManager().registerEvents(playerManager, instance);
        getDwDLogger().log("DwDCore API Enabled.");
    }
    
    @Override
    public void pluginDisable() {
        getDwDLogger().log("DwDCore API Disabled.");
    }
}
