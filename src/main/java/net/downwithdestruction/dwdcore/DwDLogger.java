/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdcore;

import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class DwDLogger {
    
    private JavaPlugin plugin;
    private ChatColor colour = ChatColor.DARK_RED;
    
    public DwDLogger(DwDPlugin plugin) {
        this.plugin = plugin;
    }
    
    public void setColour(ChatColor colour) {
        this.colour = colour;
    }
    
    public void log(String message, Level level) {
        if (plugin.getConfig().getBoolean("useFancyConsole",true) == true) {
            ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
            console.sendMessage("[" + colour + plugin.getDescription().getName() + " v" + plugin.getDescription().getVersion() + ChatColor.GRAY + "] " + message);
        } else {
            Bukkit.getLogger().log(level, "[{0} v{1}] {2}", new Object[]{plugin.getDescription().getName(), plugin.getDescription().getVersion(), message});
        }
    }

    public void log(String message) {
        log(message, Level.INFO);
    }

    public void debug(String message) {
        if (plugin.getConfig().getBoolean("debug",false) == false) {
            log(message, Level.INFO);
        }
    }
}
