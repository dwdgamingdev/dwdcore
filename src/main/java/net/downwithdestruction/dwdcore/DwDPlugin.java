/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdcore;

import com.sk89q.bukkit.util.CommandsManagerRegistration;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissionsException;
import com.sk89q.minecraft.util.commands.CommandUsageException;
import com.sk89q.minecraft.util.commands.CommandsManager;
import com.sk89q.minecraft.util.commands.MissingNestedCommandException;
import com.sk89q.minecraft.util.commands.WrappedCommandException;
import java.util.List;
import net.downwithdestruction.dwdcore.players.DwDPlayerManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class DwDPlugin extends JavaPlugin {

    private DwDLogger logger = new DwDLogger(this);
    private Language language = new Language(this);
    private CommandsManager<CommandSender> commands;
    private CommandsManagerRegistration cmdRegister;

    public DwDLogger getDwDLogger() {
        return logger;
    }

    public Language getLanguage() {
        return language;
    }

    public DwDPlayerManager getPlayerManager() {
        return DwDCore.getInstance().getPlayerManager();
    }

    @Override
    public void onEnable() {
        logger.log("Enabling " + getDescription().getFullName());
        Boolean error = false;
        List<String> depend = getDescription().getDepend();
        if (depend != null) {
            for (String plugin : depend) {
                if (!getServer().getPluginManager().isPluginEnabled(plugin)) {
                    error = true;
                    logger.log("Missing Dependency. Please install '" + plugin + "'.");
                }
            }
        }

        if (error == true) {
            logger.log("Missing Dependencies. Disabling.");
            getPluginLoader().disablePlugin(this);
            return;
        }

        getLanguage().load();
        setupCommands();
        pluginEnable();
        logger.log(getDescription().getFullName() + " enabled.");
    }

    @Override
    public void onDisable() {
        logger.log("Disabling " + getDescription().getFullName());
        pluginDisable();

        getLanguage().save();
        logger.log(getDescription().getFullName() + " disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd,
            String commandLabel, String[] args) {
        try {
            commands.execute(cmd.getName(), args, sender, sender);

        } catch (CommandPermissionsException e) {
            sender.sendMessage(ChatColor.RED
                    + getLanguage().get("error.noPermission"));
        } catch (MissingNestedCommandException e) {
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (CommandUsageException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (WrappedCommandException e) {
            if (e.getCause() instanceof NumberFormatException) {
                sender.sendMessage(ChatColor.RED
                        + getLanguage().get("error.numExpected"));
            } else {
                sender.sendMessage(ChatColor.RED
                        + getLanguage().get("error.errorOccurred"));
                e.printStackTrace();
            }
        } catch (CommandException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
        }

        return true;
    }

    private void setupCommands() {
        this.commands = new CommandsManager<CommandSender>() {
            @Override
            public boolean hasPermission(CommandSender sender, String perm) {
                return sender.hasPermission(perm);
            }
        };

        cmdRegister = new CommandsManagerRegistration(
                this, this.commands);
    }

    public void registerCommand(Class<?> cmdClass) {
        cmdRegister.register(cmdClass);
    }

    public void pluginEnable() {
    }

    public void pluginDisable() {
    }
}
