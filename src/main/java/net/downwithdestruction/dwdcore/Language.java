package net.downwithdestruction.dwdcore;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Language {

    private DwDPlugin plugin;
    private File languageFile;
    private FileConfiguration language;

    public Language(DwDPlugin plugin) {
        this.plugin = plugin;
    }

    public void load() {
        if(getLanguageFile() == null)
            setLanguageFile(new File(plugin.getDataFolder(),"language.yml"));
        saveDefault();
        setLanguage(YamlConfiguration.loadConfiguration(getLanguageFile()));

        InputStream pluginFile = plugin.getResource("language.yml");
        if (pluginFile != null) {
            YamlConfiguration pluginYaml = YamlConfiguration.loadConfiguration(pluginFile);
            getLanguage().setDefaults(pluginYaml);
        }
        save();
    }

    public void save() {
        try {
            getLanguage().save(getLanguageFile());
        } catch (IOException e) {
            plugin.getDwDLogger().log("Unable to save language.");
        }
    }
    
    public void saveDefault() {
        if (!getLanguageFile().exists()) {
         this.plugin.saveResource("language.yml", false);
     }
    }

    public String get(String node) {
        if (getLanguage().contains(node)) {
            return parse(getLanguage().getString(node));
        } else if (getLanguage().contains("error.noLanguage")) {
            return parse(getLanguage().getString("error.noLanguage"));
        } else {
            return ChatColor.RED + "Sorry, no feedback is defined for " + ChatColor.ITALIC + node + ChatColor.RESET + ChatColor.RED + ".";
        }
    }

    public String get(String node, Map<String, Object> replace) {
        String text = get(node);
        Set<String> keys = replace.keySet();
        for (String key : keys) {
            text = text.replaceAll(key, replace.get(key).toString());
        }

        return text;
    }

    private static String parse(String str) {
        if (str == null) {
            str = "";
        }
        Pattern color_codes = Pattern.compile("&([0-9A-Fa-fKkLlOoMmNn])");
        Matcher find_colors = color_codes.matcher(str);
        while (find_colors.find()) {
            str = find_colors.replaceFirst(new StringBuilder().append(ChatColor.COLOR_CHAR).append(find_colors.group(1)).toString());
            find_colors = color_codes.matcher(str);
        }
        return str;
    }
    
    private void setLanguageFile(File file) {
        this.languageFile = file;
    }
    
    private void setLanguage(YamlConfiguration configuration) {
        this.language = configuration;
    }
    private File getLanguageFile() {
        return languageFile;
    }
    
    private FileConfiguration getLanguage() {
        return language;
    }
}
