package net.downwithdestruction.dwdcore.players;

import java.io.File;
import java.io.IOException;
import net.downwithdestruction.dwdcore.DwDCore;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class DwDPlayer {
    
    private boolean isOnline = false;
    private String name = "Player";
    
    private FileConfiguration tempData = new YamlConfiguration();
    private FileConfiguration permData = new YamlConfiguration();
    
    private Player onlinePlayer;
    private OfflinePlayer offlinePlayer;
    
    public DwDPlayer(String player) {
        if(DwDCore.getInstance().getPlayerManager().playerOnline(player)) {
            createFromPlayer(Bukkit.getPlayer(player));
        }
        else {
            createFromOfflinePlayer(Bukkit.getOfflinePlayer(player));
        }
    }
    
    public DwDPlayer(Player player) {
        createFromPlayer(player);
    }
    
    public DwDPlayer(OfflinePlayer player) {
        createFromOfflinePlayer(player);
    }
    
    private void createFromPlayer(Player player) {
        this.onlinePlayer = player;
        this.offlinePlayer = null;
        this.isOnline = true;
        this.name = player.getName();
        load();
    }
    
    private void createFromOfflinePlayer(OfflinePlayer player) {
        this.onlinePlayer = null;
        this.offlinePlayer = player;
        this.isOnline = false;
        this.name = player.getName();
        load();
    }
    
    private File getFile() {
        return new File(DwDCore.getInstance().getDataFolder().getAbsolutePath() + File.separatorChar + "players" + File.separatorChar + name + ".yml");
    }
    
    public FileConfiguration getTempData() {
        return tempData;
    }
    
    public FileConfiguration getData() {
        return permData;
    }
    
    public void load() {
        permData = YamlConfiguration.loadConfiguration(getFile());
    }
    
    public void save() {
        try {
            permData.save(getFile());
        }
        catch(IOException e) {
            DwDCore.getInstance().getDwDLogger().log("Unable to save player data for '" + name + "'");
        }
    }

    public String getName() {
        return name;
    }
    
    public Player getPlayer() {
        return onlinePlayer;
    }
    
    public OfflinePlayer getOfflinePlayer() {
        return offlinePlayer;
    }
    
}
