package net.downwithdestruction.dwdcore.players;

import java.util.HashMap;
import java.util.Map;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class DwDPlayerManager implements Listener {
    
    private Map<String,DwDPlayer> players = new HashMap<>();
    
    public boolean playerOnline(String player) {
        if(players.containsKey(player)) {
            return true;
        }
        return false;
    }
    
    public DwDPlayer getPlayer(String player) {
        if(players.containsKey(player)) {
            return players.get(player);
        }
        return null;
    }
    
    @EventHandler(priority=EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        players.put(event.getPlayer().getName(), new DwDPlayer(event.getPlayer()));
    }
    
    @EventHandler(priority=EventPriority.MONITOR)
    public void onPlayerKick(PlayerKickEvent event) {
        players.remove(event.getPlayer().getName());
    }
    
    @EventHandler(priority=EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        players.remove(event.getPlayer().getName());
    }
    
}
